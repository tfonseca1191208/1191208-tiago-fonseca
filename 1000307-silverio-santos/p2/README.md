ISEP - **Silvério Santos** - 1000307 - P2
=========================================

## 0. Tabela de conteúdos

 1. Análise de requisitos 
 2. Pressupostos
 3. Instalação do TODD
 4. Ativação do JMX no tomcat
 5. Integração do JMX com o nagios
 6. Recuperação do TODD caso necessário
 7. Percentagem de sessões disponiveis no todd via polling
 8. monitorização do todd via passive checks
 9. jmx notifications (events)
 10. monitorização do tomcat catalina 
 11. Alternativas ao JMX (WMI)
 12. Conclusão

### 1 - Análise dos requisitos

Pretende-se extender a monitorização efetuada no projeto P1, fazendo com que a ferramenta nagios possa monitorizar parametros especificos e internos de certas aplicações, mais concretamente parametros memoria, sessões e conexões de duas aplicações distintas, o tomcat e o TODD. Pretende-se ainda que sejam feitos ajustes a estas aplicações em tempo de execução, caso se revele necessário, ou seja, alterar parametros por exemplo de memoria disponivel ou numero de sessoes máximas caso as aplicações em causa estejam sobrecarregadas. Em casos extremos, a ferramenta de monitorização deve procurar reiniciar a/as aplicações em causa com parametros diferentes, caso necessário. Para efetur esta monitorização é necessário recorrer à ferramenta JMX.
Nesta nova monitorização pretende-se que existam verificações passive e active (polling) consoante requisitos.
Pretende-se ainda comparar a ferramenta JMX com o WMI.

**Podemos então dividir este sprint nos seguintes grandes tópicos:**
Integração do nagios com JMX
Passive / Active checks
Criação de uma aplicação para monitorização via JMX

### 2 - Pressupostos
Pressupõe-se que a ferramenta nagios está previamente instalada numa máquina virtual, e a máquina a ser monitorizada está já com o tomcat instalado.

### 3 - Instalação do TODD
O TODD é um serviço simples que retorna dados basicos do servidor onde corre, como por exemplo o current timestamp.
A instalação deste serviço requer os seguintes comandos:

    cd /target/directory
    sudo rm -r todd
    sudo git clone https://1000307@bitbucket.org/mei-isep/todd.git

**Uma vez que esta sequencia é corrida numa config vagrant, elimina-se primeiro a pasta todd caso exista para que o vagrant não bloqueie!**
#### de seguida substituimos o codigo fonte do todd com as nossas alterações personalizadas
	sudo cp /vagrant/todd-cfg/mySrcChanges/* /home/vagrant/todd/src/main/java/net/jnjmx/todd/
	sudo chmod 755 /home/vagrant/todd/src/main/java/net/jnjmx/todd/*
Para executar o serviço recorremos à ferramenta gradle, a qual é instalada com

	sudo apt-get install gradle
	
A execução é feita da seguinte forma:

	cd /home/vagrant/todd/
	sudo echo "starting runServerRemote"
	sudo gradle runServerRemote & disown
	
Usamos o parametro "& disown" para que o arranque através da config vagrant não bloqueie neste ponto

### 4 - Ativação do JMX no tomcat
Para podermos monitorizar parametros especificos do tomcat é necessário proceder à ativação do serviço JMX aquando do arranque da JVM, para isso é necessário criar um ficheiro setenv.sh
com a seguinte instrução:

    CATALINA_OPTS="-Dcom.sun.management.jmxremote -Dcom.sun.management.jmxremote.port=9000 -Dcom.sun.management.jmxremote.rmi.port=9000 -Dcom.sun.management.jmxremote.ssl=false -Dcom.sun.management.jmxremote.authenticate=false -Djava.rmi.server.hostname=192.168.56.11 -Dcom.sun.management.jmxremote.host=192.168.56.11"


### 5 - Integração do jmx com o nagios
Para monitorizar estes parametros a partir do nagios recorremos ao plugin check_jmx. A instalação deste plugin na máquina que corre o nagios é feita da seguinte forma:

	cd /tmp
	sudo wget "https://exchange.nagios.org/components/com_mtree/attachment.php?link_id=1274&cf_id=24" -O check_jmx.tgz
	sudo tar xzf check_jmx.tgz
	sudo chmod -R +x /tmp/check_jmx/nagios/plugin/*
	sudo chown -R nagios:nagios /tmp/check_jmx/nagios/plugin/*
	sudo cp /tmp/check_jmx/nagios/plugin/* /usr/local/nagios/libexec

Este plugin usa o plugin previamente existente NRPE para consultar dados na máquina destino através so JMX. Para isso temos que adicionar às configurações do NRPE, um comando com a instrução seguinte:

    command[check_jmx]=/usr/lib/nagios/plugins/check_jmx -U service:jmx:rmi:///jndi/rmi://192.168.56.11:9000/jmxrmi -O java.lang:type=Memory -A HeapMemoryUsage -K used -I 

Pode também ser usado diretamente sem passar pelo NRPE, para isso adicionamos ao nagios um comando deste genero:

    define command{
     command_name check_todd_jmx
     command_line /usr/local/nagios/libexec/check_jmx -U service:jmx:rmi:///jndi/rmi://192.168.56.11:10500/jmxrmi -O java.lang:type=Memory -A HeapMemoryUsage -K used -I HeapMemoryUsage -J used -vvvv -w 4248302272 -c 5498760192
    }

### 6 - restaurar o todd caso necessário
O facto deste plugin check_jmx poder ser usado diretamente ou em conjunto com o NRPE permite-nos outras utilidades, como por exemplo executar scripts personalizados na máquina monitorizada.
Neste caso recorremos a este método para tentar reiniciar o TODD caso necessário.
para isto adicionamos ao NRPE o seguinte comando:
command[checar_todd]=/etc/nagios/check_todd.sh $ARG1$ $ARG2$
Este comando executa o shell script check_todd.sh com os parametros que lhe passarmos. Este script deverá estar previamente instalado na maquina monitorizada recorrendo ao comando

	sudo cp /vagrant/check_todd.sh /etc/nagios/check_todd.sh
	sudo chmod +x /etc/nagios/check_todd.sh	

podemos assim invocar esta verificação da seguinte forma:

    define service{
    	use generic-service
    	host_name tomcat
    	service_description toddViaJMX
    	check_command check_todd_jmx
    	event_handler check_nrpe!checar_todd -a '$SERVICESTATE$ $SERVICESTATETYPE$ $SERVICEATTEMPT$'
    }

Este check_todd.sh é similar à verificação uqe fizemos na entrega P1 em que tentamos reiniciar serviços caso estejam em critical hard state ou na 3ª vez critical soft state.

### 7 - Percentagem de sessões disponiveis no todd via polling
Para monitorizar no nagios a percentagem de sessões disponiveis no TODD é necessário aceder a duas métricas, as sessões disponiveis e o numero maximo de sessões (size).
Para isso criamos os seguintes serviços:

    define service{
    	use generic-service
    	host_name tomcat
    	service_description toddSessionsSize
    	check_command check_todd_size
    }
    define service{
    	use generic-service
    	host_name tomcat
    	service_description toddSessionsAvailable
    	check_command check_todd_available!7!6
    }

Isto permite-nos ter as duas metricas como macros, as quais são usadas da seguinte forma:

    define service{
    	use generic-service
    	host_name tomcat
    	service_description toddSPercentAvailable
    	check_command check_todd_percent!$SERVICEOUTPUT:tomcat:toddSessionsAvailable$!$SERVICEOUTPUT:tomcat:toddSessionsSize$ -w 30 -c 20
    	notifications_enabled        	1
       	notification_period          	24x7
       	notification_interval        	120
       	notification_options         	c
       	contacts                     	nagiosadmin
    }

Este comando check_todd_percent recebe assim dois inputs e retorna uma percentagem. Como estes dois inputs são strings que contêm texto e o numero que pretendemos, criei um pequeno scrypt python que usa uma expressão regular para retirar das strings os valores que nos interessam. 
Note-se que este script não efetua validações e é suscetivel a erros pelo que não deve ser usado desta forma em ambiente real!

Este serviço toddSPercentAvailable permite-nos assim monitorizar a percentagem de sessoes disponiveis e enviar notificações ao admin caso desça abaixo dos 20%.


### 8 - monitorização do todd via passive checks
Foi-nos solicitado que fizessemos esta mesma monitorização recorrendo a passive checks. Isto significa que em vez de o nagios estar sempre a solicitar o estado das aplicações (método polling), o nagios fica à escuta que alguém o informe dessas atualizações (passive checks).
Para solucionar este problema recorremos ao plugin NRDP (Nagios remote data processor). O que este plugin faz é colocar um daemon à escuta de inputs, os quais grava na bd do nagios.
A instalação deste plugin requer os seguintes comandos:

	sudo apt-get install -y php-xml
	cd /tmp
	sudo wget -O nrdp.tar.gz https://github.com/NagiosEnterprises/nrdp/archive/2.0.3.tar.gz
	sudo tar xzf nrdp.tar.gz
	cd /tmp/nrdp-2.0.3/
	sudo mkdir -p /usr/local/nrdp
	sudo cp -r clients server LICENSE* CHANGES* /usr/local/nrdp
	sudo chown -R nagios:nagios /usr/local/nrdp
	sudo cp nrdp.conf /etc/apache2/sites-enabled/
	
O envio de informação para este plugin através de aplicações externas requer previamente que elas tenham permissões para o fazer, para isso o NRDP obriga a que conheçam um token pre-definido como método de autenticação. Os tokens a ser usados devem ser adicionados ao ficheiro config.inc.php. No nosso caso temos esta configuração previamente escrita e copiamo-la para a maquina quando é ligada através do vagrant:
	sudo cp /vagrant/config.inc.php /usr/local/nrdp/server/config.inc.php 
terminamos a instalação reiniciando o apache:
	sudo systemctl restart apache2.service

O plugin pode ser testado acedendo via browser, no nosso caso a http://127.0.0.1:8080/nrdp/

Podemos agora enviar "updates" ao nagios através de qualquer aplicação. No nosso caso, por questões de simplicidade e sendo este um trabalho académico, optamos por usar uma das muitas ferramentas disponibilizadas pelos autores do proprio plugin: um script python (send_nrdp.py) que recebe os parametros que queremos comunicar e transmite-os à máquina de monitorização.

### 9 - JMX notifications (events)
Foi-nos solicitado o desenvolvimento de uma aplicação para monitorização do TODD recorrendo ao mecanismo de eventos do JMX, ou seja, a nossa aplicação deverá definir parametros de monitorização e ficar à espera de ser notificada caso esses parametros sejam excedidos.

Para solucionar este problema começamos com a base desenvolvida na app TODD em runClient3 e criamos a nossa classe "runClient4" que define um gaugeMonitor que implementa um listener que é invocado caso a percentagem de sessoes disponiveis desça abaixo dos 20%. Neste caso o listener é responsavel por invocar a operação "grow", aumentando assim a session pool size.
Para alem disto informamos o nagios através do NRDP de que o todd está neste situação e de que fizemos esta operação.


### 10 - monitorização do tomcat catalina
Da mesma forma, usamos a classe "runCLient4" para implementar um monitor que verifica o estado da heapMemoryUsage e caso atinja um threshold critico o listener invoca o metodo "gc" (garbage collector). Caso não seja suficiente, a app procura aumentar a memoria disponibilizada ao java através da instrução "export _JAVA_OPTIONS="-Xmx2G"" e reinicio do tomcat. 
Esta aplicação envia ainda periodicamente ao nagios informação sobre o estado do catalina.

### 11 - Alternativas ao JMX: Windows Management Instrumentation
Foi-nos solicitado que estudassemos uma alternativa ao JMX, mais concretamente o WMI.
O WMI é um "equivalente" ao JMX mas destinado a tecnologias Microsoft .Net, ou seja, trata-se de uma ferramenta que permite monitorizar aplicações que requerem ser corridas em ambiente Windows.

Para monitorizar uma aplicação deste género a partir do nagios, podemos por exemplo recorrer ao plugin check wmi.
Para instalar e configurar esta ferramenta devem seguir-se os seguintes passos:

**Preparar o sistema monitorizado**  
No sistema windows a monitorizar:

-   Criar um utilizador para usar o Check WMI Plus
-   Remover permissões de login. Adicionar privilegios de Domain Admin. 

**Prerequisitos**  
Colocar o Zenoss wmic a funcionar.  

-   Download a partir da  [fonte](https://edcint.co.nz/checkwmiplus/download/zenoss-wmi-source-v1-3-14/)
-   Compilar e instalar.

Testar o wmic.  
Comando: 
 `/bin/wmic -U USER%PASS //HOST 'Select Caption From Win32_OperatingSystem'`  
O output deverá ser algo do género:  
`CLASS: Win32_OperatingSystem  
Caption|Name  
Microsoft Windows XP Professional|Microsoft Windows XP Professional|C:\WINDOWS|\Device\Harddisk0\Partition1`

**Instalar o plugin**  
[Descarregar](https://edcint.co.nz/checkwmiplus/releases/) a versao mais recente do plugin

Descompactar o ficheiro para uma pasta temporaria.  
Copiar os executaveis para /usr/local/bin.  
Copiar a pasta etc deste bundle para /etc. Esta pasta é o default para os ficheiros config e ini.

Renomear check_wmi_plus.conf.sample para check_wmi_plus.conf  

Não esquecer de dar permissoes de leitura aos ficheiros config ao user nagios.

Resta-nos então criar nas configs do nagios os hosts e serviços a serem monitorizados.

Finalmente devemos customizar os nossos checks no mycustom.ini


### 12 - Conclusão
Foram cumpridos todos os pontos solicitados. A escolha entre passive ou active checks no nagios deve ser ponderada caso a caso. Neste caso implementou-se das duas formas por uma questão de demonstração mas nalguns casos faz mais sentido a utilização de um método em relação ao outro, e devem ter-se em conta por exemplo questões de segurança ou overhead.
