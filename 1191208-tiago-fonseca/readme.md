# P3 - Virtualization and Containers

# Introduction
The theme of the current exercise is Virtualization and Containers. The objective of this assignment is using Docker instead of virtual machines. The service for the monitoring of the systems should be Nagios. The scenario that should be implemented is the monitorization with a docker container with Nagios of another two containers, one with Tomcat and another one with Todd sample application.

## Create docker nagios container
To configure the nagios container, it is necessary to start creating a folder named nagios and inside the folder, create a file called dockerfile with the code below.

== BEGIN - DOCKERFILE CONFIGURATION FOR NAGIOS CONTAINER ==

FROM jasonrivers/nagios
RUN apt-get update
RUN apt-get -y install openjdk-8-jdk sendmail wget  git ssh nano unzip nsca gradle iputils-ping sudo sasl2-bin libsasl2-modules rsyslog 
RUN mkdir /home/master/
RUN cd /home/master && git clone https://tfonseca1191208@bitbucket.org/tfonseca1191208/todd.git
RUN cd /home/master/todd && gradle jar && gradle build

== END - DOCKERFILE CONFIGURATION FOR NAGIOS CONTAINER ==

In this assignment, the todd application will be implement in a different container from tomcat, in contrary of the last assignment. For this, it is necessary to update remoteMonitoring.cfg. There is the requirement of add a new host for todd and also change the ip of the monitored tomcat to match with the ip of the tomcat container. For this, replace the code of the remoteMonitoring.cfg of P2 assignment with the code below and copy that file to a nagiosFile folder inside of this directory.

== BEGIN - REMOTEMONITORING.CFG CONFIGURATION ==
# Define a host for the local machine

###############################################################################
#
# remoteMonitoring.CFG - CONFIG FILE FOR MONITORING THE REMOTE MACHINE
#
###############################################################################
###############################################################################




###############################################################################
###############################################################################
#
# HOST DEFINITION
#
###############################################################################
###############################################################################

# Define a host for the local machine

define host {

    use                     linux-server    
    host_name               tomcat
    alias                   tomcat
    address                 tomcat
    notifications_enabled   1 
    contact_groups          Group1
    notification_options    d,r
    notification_interval   960
    notification_period     24x7
    flap_detection_enabled  0
    passive_checks_enabled  1
}

define host {

    use                     linux-server    
    host_name               todd
    alias                   todd
    address                 todd
    notifications_enabled   1 
    contact_groups          Group1
    notification_options    d,r
    notification_interval   960
    notification_period     24x7
    flap_detection_enabled  0
    passive_checks_enabled  1
}

###############################################################################
###############################################################################
#
# SERVICE DEFINITIONS
#
###############################################################################
###############################################################################


# Define a service to "ping" the local machine

define service{
        use                             local-service         ; Name of service template to
        host_name                       tomcat,todd
        service_description             PING
        check_command                   check_ping!100.0,20%!500.0,60%
        notifications_enabled           1
        notification_options            w,u,c,r
}


# Define a service to check the disk space of the root partition
# on the local machine.  Warning if < 20% free, critical if
# < 10% free space on partition.

define service{
        use                             local-service         ; Name of service template to$        
        host_name                       tomcat,todd
        service_description             Root Partition
        check_command                   check_nrpe!check_disk
        }

# Define a service to check the number of currently logged in
# users on the local machine.  Warning if > 20 users, critical
# if > 50 users.

define service {
        use                     local-service           ; Name of service template to use
        host_name               tomcat,todd
        service_description     Current Users
        check_command           check_nrpe!check_users
        check_interval          1
}



# Define a service to check the number of currently running procs
# on the local machine.  Warning if > 250 processes, critical if
# > 400 processes.

define service {
    use                     local-service           ; Name of service template to use
    host_name               tomcat,todd
    service_description     Total Processes
    check_command           check_nrpe!check_procs
    check_interval          1
}



# Define a service to check the load on the local machine.

define service {
    use                     local-service           ; Name of service template to use
    host_name               tomcat,todd
    service_description     Current Load
    check_command           check_nrpe!check_load
    check_interval          1
}



# Define a service to check the swap usage the local machine.
# Critical if less than 10% of swap is free, warning if less than 20% is free

define service {

    use                     local-service           ; Name of service template to use
    host_name               tomcat,todd
    service_description     Swap Usage
    check_command           check_nrpe!check_swap
    check_interval          1
}



# Define a service to check SSH on the local machine.
# Disable notifications for this service by default, as not all users may have SSH enabled.

define service {

    use                     local-service           ; Name of service template to use
    host_name               tomcat,todd
    service_description     SSH
    check_command           check_ssh
    notifications_enabled   0
}


# My tomcat HTTP service on port 8080

define service {
        use                             local-service
        host_name                       tomcat
        service_description             Tomcat
        check_command                   check_http!-p 8080
        check_interval                  1
        contacts                        nagiosadmin, tiago
        contact_groups                  admins
        notifications_enabled           1
        notification_options            w,u,c,r
        event_handler_enabled           1
        event_handler                   restart-tomcat
        max_check_attempts              4
       
}

# My todd up service

define service {

        use                             local-service
        host_name                       todd
        service_description             todd-up
        check_command                   check_todd_up
        check_interval                  1
        contacts                        nagiosadmin, tiago     
        contact_groups                  admins
        notifications_enabled           1
        notification_options            w,u,c,r
        event_handler_enabled           1
        event_handler                   restart-toddServer
        max_check_attempts              4 
                  
}

# My todd load service

define service {

        use                             local-service
        host_name                       todd
        service_description             todd-load
        check_command                   check_todd_load
        check_interval                  1
        max_check_attempts              4
        contacts                        nagiosadmin, tiago         
}


define service {
        use                             passive-service
        host_name                       todd        
        service_description             passive-service-todd
        max_check_attempts              1
        event_handler_enabled           1
        event_handler                   increase-todd        
        _grow_number                    12        
}

define service{
        use                             passive-service
        host_name                       todd        
        service_description             passive-load-tomcat
        max_check_attempts              1
        event_handler_enabled           1
        event_handler                   restart-tomcat
}

== END - REMOTEMONITORING.CFG CONFIGURATION ==

To guarantee that nagios is working correctly, it is possible to open any browser and navigate to http:localhost:1234/nagios with username nagiosadmin and password nagios. If we navigate to the hosts options, we will see that the only host is called "localhost", that represents the nagios container itself.

## Create dockerfile for tomcat container
To configure the monitored docker container with tomcat, there is the necessity to create a folder called tomcat with a file called dockerfile inside. But, before of that, we should change the nrpe.cfg file to this container be able to accept connections from nagios server. For this, update the mentioned file with the following line:
- allowed_hosts=127.0.0.1,nagios

Now, the mentioned dockerfile for the tomcat machine:

== BEGIN - DOCKERFILE CONFIGURATION FOR THE TOMCAT CONTAINER ==

FROM ubuntu:latest
RUN apt-get -y update && apt-get -y upgrade
RUN apt-get -y install openjdk-8-jdk wget ssh nagios-nrpe-server mcollective-plugins-nrpe vim ssh sudo gradle
RUN mkdir /usr/local/tomcat
RUN wget http://apache.stu.edu.tw/tomcat/tomcat-8/v8.5.54/bin/apache-tomcat-8.5.54.tar.gz -O /tmp/tomcat.tar.gz
RUN cd /tmp && tar xvfz tomcat.tar.gz
RUN cp -Rv /tmp/apache-tomcat-8.5.54/* /usr/local/tomcat/
COPY tomcat-users.xml /usr/local/tomcat/conf/tomcat-users.xml
COPY context.xml /usr/local/tomcat/webapps/manager/META-INF/context.xml
COPY sudoers /etc/
COPY restart_tomcat /usr/local/bin/
COPY restart_tomcat_now /usr/local/bin/
EXPOSE 8080 5678
CMD service nagios-nrpe-server start && service ssh start && /usr/local/tomcat/bin/catalina.sh run

== END - DOCKERFILE CONFIGURATION FOR THE TOMCAT CONTAINER ==

To verify if the container is working correctly, we can open the browser and navigate to the follow url: https:localhost:5678/tomcat and we should see a tomcat page.

## Create docker todd container
To configure the monitored docker container with the todd application, there is the necessity to create a folder called tomcat with a file called dockerfile inside. But, before of that, we should change the nrpe.cfg file to this container be able to accept connections from nagios server. For this, update the mentioned file with the following line:
- allowed_hosts=127.0.0.1,nagios

Now, the mentioned dockerfile for the todd container:

== BEGIN - DOCKERFILE CONFIGURATION FOR THE TODD CONTAINER ==

FROM ubuntu:latest
RUN apt-get -y update && apt-get -y upgrade
RUN export DEBIAN_FRONTEND=noninteractive
RUN apt-get install -y tzdata
RUN ln -fs /usr/share/zoneinfo/Europe/Portugal /etc/localtime
RUN dpkg-reconfigure --frontend noninteractive tzdata
RUN apt-get -y install openjdk-8-jdk gradle wget git ssh nagios-nrpe-server nsca iptables nagios-plugins vim libssl-dev nsca sudo
COPY sudoers /etc/
COPY restart_toddServer /usr/local/bin/
RUN mkdir /home/master/
RUN cd /home/master && git clone https://tfonseca1191208@bitbucket.org/tfonseca1191208/todd.git
RUN cd /home/master/todd && gradle build
CMD service ssh start && service nagios-nrpe-server start && cd /home/master/todd && gradle runServerRemote

== END - DOCKERFILE CONFIGURATION FOR THE TODD CONTAINER ==

## Create a docker-compose
To finish all the work and to run all the containers together, we can either link them on the command line or, to make our work easy for the next times, we can use a docker compose file and link all the containers there. So, create a file called docker-compose.yaml with the code below:

== BEGIN - DOCKERCOMPOSE CONFGIURATION FILE ==

version: "3"
services:
    nagios:
        build: nagios
        ports:
            - "1234:80"
        volumes:
            - ./nagiosFiles/etc:/opt/nagios/etc/
            - ./nagiosFiles/libexec:/opt/nagios/libexec/
    todd:
        build: todd
        ports: 
            - "10500:10500"
            - "3006:3006"
        volumes:
            - ./toddFiles:/etc/nagios/
    tomcat:
        build: tomcat
        ports:
            - "10501:10501"
            - "5678:8080"
        volumes:
            - ./tomcatFiles:/etc/nagios/

== END - DOCKERCOMPOSE CONFIGURATION FILE == 

As is possible to see, in the above docker-compose.yaml, 3 containers were created: nagios, tomcat and todd. 

The container created for nagios has as base image the jsonrivers/nagios:latest and uses 1234:80 for nagios, 6002:6002 for JMX todd and 5666 for NRPE. The files that belong to the nagios are configured in the ./nagiosFiles/ as a docker volume.

The container created for tomcat has as base image the ubuntu:latest and uses 5678:8080 as network port for the tomcat, 6002:6002 for JMX and 5666 as the NRPE port. The files that belong to the tomcat are configured in the ./tomcatFiles/ as a docker volume.

The container created for tood has as base image the ubuntu:latest and uses 6002:6002 for JMX and 5666 as the NRPE port. The files that belong to the todd are configured in the ./toddFiles/ as a docker volume.

Now, everything is ready to go. We should open a terminal and right "docker-compose up". It will create all the containers already configured. This action may take some time to finish.

To verify that all is working, we can go to the browser and navigate to the followinng url: "https:localhost:1234/nagios/", after this select the hosts option in left side menu and we will be able to see the machines that are being monitored.