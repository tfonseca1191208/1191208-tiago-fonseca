**Hugo Cadilhe** (1160887) - P1
===============================
# Problem Analysis

## JMX

The objective is to implement application monitoring using JMX. For this the previous Nagios work to monitor the system will be used.
Regarding alternative tools for this WMI (Windows Management Instrumentation) was also analysed.

### Integration with Nagios
You should integrate the system monitoring tools (e.g., Nagios) with JMX monitoring
The system monitoring server (e.g., Nagios) should be able to remotely monitor the TODD server.
It should be able to:
 * Check (polling) if TODD server is running. If TODD server is not running you should try to automatically start it.
 * Check (polling) if the "AvailableSessions" attribute is below 20% of the "Size" attribute. If so, you should consider that the service is critical and notify someone.

### JMX Notifications
* You should develop a JMX Agent Application that is able to use JMX notifications to react to "events" in a JMX monitored resource.
* Using JMX notifications this application should be able to monitor the TODD server and increase the size of the "SessionPool" by using the method "grow" when the "AvailableSessions" attribute is below 20% of the "Size" attribute. You should try to integrate with the system monitoring tools using passive checks.
* You should also use a similar approach to handle possible JVM memory limitations regarding the Tomcat server. You should define thresholds for this concern and try to free memory or even restart tomcat if necessary (with different settings for the memory usage of the JVM).

# Design
The design of this solution was based around the existing solution to the previously devoloped P1 project.

Vagrant was used to implement the two virtual machines created, to comunicate with each other, using VirtwalBox to generate them.
JMX will be responsible for managing the suplied example application, TODD (Time Of Day Deamon), which will be monitored by remotly monitored by nagios. This application executes a simpole action, accepts client connections and sends them the current date and time.

Nagios configuration was updated to fulfill the requests presented in Problem Analysis

# Implementation
First we need to install the applications requirements, Java JDK and Gradle, since Java is also a requirement for Gradle it is installed first.
```
sudo apt-get install -y opendk-8-jdk
```
Folllowed by:
```
sudo apt-get install -y gradle
```

Now we need to download the TODD aplication to both machines, for this we will use GIT to clone the application and create a directory for the application with the correct permissions, all permissions for the owner and red/execute for other users.
```
sudo apt-get install -y git
sudo mkdir TODD
sudo chmod 0755  TODD
cd /TODD
sudo git clone https://bitbucket.org/mei-isep/todd/src/master/
```

For the monitoring machine's TODD client to connect to the monitored machine, the application needs to know the other machine's ip address. In order to do that we need to change the ip address in the file build.gradle to the ip of the monitored machine.
Next we need to update build.gradle's permissions to allow for the owner to to everything and allow reading from others. After updating the necessary files we need to build the application using gradle.
```
sudo chmod 744 /TODD/master/build.gradle
cd /TODD/master/
sudo gradle jar
sudo gradle build
```

# Alternative

WMI is a tool that allows the user obtain data and perform operations from remote computers, allowing the user to manage several workstations remotly.
This tool is based on WinRM (Windows Remote Management Service) and has a focus on administrative scripts and corporate applications.


