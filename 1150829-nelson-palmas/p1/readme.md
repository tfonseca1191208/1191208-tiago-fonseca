# P1.1 - Monitoring Networks and Systems

# Problem Analysis

## 1 - Remote Monitoring

* The monitoring of some properties (e.g., disk free space) may require the installation of software in the monitored machine

* With Nagios one of the solutions for this issue is to use NRPE

* The monitoring can be done by "polling" or by "pushing". These may also be known as active or passive checks

## 2 - Automatic Recovery

* You should install Tomcat in the monitored machine

* The Monitoring Server should try to automatically recover the Tomcat server when it is down. In Nagios, please refer to "event handlers" to support your solution

* Contact(s) should be notified by email when the service changes states (e.g., up, down, etc,). In Nagios you may use the sendemail.

## 3 - Customization

* How the monitoring tool can be customized (e.g., using a different database, adding new features/plugins, setting different compiling options for optimization or security purposes, for instance, regarding NRPE)

# Nagios

## Design

The test environment was set up to be a virtual machine created using the provider Virtualbox. The purpose of this is to setup the VM's and their environment using a Vagrant file. This file allows you to introduce shell scripts and also setup several properties of the VM's.

The machines were created with the default configuration:
 * One NAT adapter and a Host-only adapter
 * They were created with the image ubuntu-xenial
 * 512 MB Ram
 * 10 GB HDD
 * Ubuntu 16.04
 
In order to do this assignement, we needed one "main" machine and other machines to be monitored with NRPE. I decided to use only one machine to be monitored.
 
In the monitoring machine, it was installed nagios core with the plugins needed and its dependencies.
 
In the monitored machine, tomcat 7 was installed, and set up at port 8080 (default).
 
The monitoring machine was defined to monitor the following monitored machines services:
 
 * PING
 * Current Load Guest
 * Current Users Guest
 * HTTP-80
 * Check Swap Guest
 * HTTP-Tomcat-8080
 * Root Partition Guest
 * SSH
 
Notifications were set for all the services refered in the previous point and e-mails were sent to the contacts defined.

Auto recovery for the service http-tomcat-8080 was implemented so that every time the state of the service changed to CRITICAL and it was the second time, an request to restart the tomcat server was sent through a tunnel using NRPE. This makes the monitored machine to do the command "sudo service tomcat restart".

## Nagios & NRPE

### NRPE Monitoring 

You can monitor several properties remotelly with NRPE, like cpu load, disk space, http, etc. 

The NRPE addon consists of two pieces:
 
 * The check_nrpe plugin, which resides on the local monitoring machine
 * The NRPE daemon, which runs on the remote Linux/Unix machine

When Nagios needs to monitor a resource of service from a remote Linux/Unix machine:

 * Nagios will execute the check_nrpe plugin and tell it what service needs to be checked
 * The check_nrpe plugin contacts the NRPE daemon on the remote host over an (optionally) SSL protected connection
 * The NRPE daemon runs the appropriate Nagios plugin to check the service or resource
 * The results from the service check are passed from the NRPE daemon back to the check_nrpe plugin, which then returns the check results to the Nagios process.

## Customization of Nagios 

### Security Considerations:

 * Dont run nagios as root -  You can tell Nagios to drop privileges after startup and run as another user/group by using the nagios_user and nagios_group directives in the main config file.
 * Lock Down the Check Result Directory - Make sure that only the nagios user is able to read/write in the check result path.
 * Hide Sensitive Information With $USERn$ Macros - The CGIs read the main config file and object config file(s), so you don't want to keep any sensitive information in there. If you need to specify a username and/or password in a command definition use a $USERn$ macro to hide it. $USERn$ macros are defined in one or more resource files.
 * Secure Access to Remote Agents. Make sure you lock down access to agents (like NRPE) on remote systems using firewalls, access lists, etc.

 ![](https://assets.nagios.com/downloads/nagioscore/docs/nagioscore/4/en/images/security1.png)

 * Secure Communication Channels - Make sure you encrypt communication channels between different Nagios installations and between your Nagios servers and your monitoring agents whenever possible.
 
### command_line $USER1$/check_nrpe -H $HOSTADDRESS$ -c $ARG1$

 ![](https://assets.nagios.com/downloads/nagioscore/docs/nagioscore/4/en/images/security2.png)

# Alternative comparison (Zabbix)

## Zabbix Software Overview

Zabbix is also an open-source monitoring software for diverse componentes such as networks, servers, virtual machines (VMs) and cloud services. Zabbix provides monitoring metrics, among others network utilization, CPU load and disk space consumption. Zabbix monitoring configuration can be done using XML based templates which contains elements to monitor. It can use MySQL, MariaDB, PostgreSQL, SQLite, Oracle or IBM DB2 to store data. Its backend is written in C and the web frontend is written in PHP.

## Key Features

* High performance, high capacity (able to monitor hundreds of thousands of devices).
* Auto-discovery of servers and network devices and interfaces.
* Low-level discovery, allows to automatically start monitoring new items, file systems or network interfaces among others.
* High-level (business) view of monitored resources through user-defined visual console screens and dashboards.
* Web-based interface.
* JMX monitoring
* Flexible e-mail notification on predefined events.

## Nagios Core vs Zabbix comparison

Nagios and Zabbix are two of the most popular monitoring systems softwares.
One software can be more interesting than the other, depending on who is using it and for what.
Below is a side-by-side comparison tabble that compares the key componentes of both monitoring solutions, according the site https://www.comparitech.com/net-admin/nagios-vs-zabbix/#Dashboard_and_User_Interface

![ZabbixVsNagios](images/ZabbixVsNagios.PNG)

## Remote Monitoring

### In the monitored machine:

1 - Download and install NRPE 

```sudo wget http://assets.nagios.com/downloads/nagiosxi/agents/linux-nrpe-agent.tar.gz ```

```sudo tar xzf linux-nrpe-agent.tar.gz```

```cd linux-nrpe-agent```

```./fullinstall```
 
* Introduce the ip of the nagios manager machine.

2 - Uncomment the following lines from /usr/local/nagios/etc/nrpe.cfg 

```bash
command[check_users]=/usr/local/nagios/libexec/check_users -w $ARG1$ -c $ARG2$
command[check_load]=/usr/local/nagios/libexec/check_load -w $ARG1$ -c $ARG2$
command[check_disk]=/usr/local/nagios/libexec/check_disk -w $ARG1$ -c $ARG2$ -p $ARG3$
command[check_procs]=/usr/local/nagios/libexec/check_procs -w $ARG1$ -c $ARG2$ -s $ARG3$
```

### In the monitoring machine:

1 - Download nrpe and install
``` wget https://github.com/NagiosEnterprises/nrpe/releases/download/nrpe-3.2.1/nrpe-3.2.1.tar.gz ```
```sudo tar xzf nrpe-3.2.1.tar.gz```

2 - In folder nrpe-3.2.1 do:
```./configure```
```make check_nrpe```
```make install-plugin```

3 - Execute the check_nrpe command with the ip of the monitored machine (should output the version of NRPE)
```sudo /usr/local/nagios/libexec/check_nrpe -H 192.168.33.11```

4 - Create a command in command.cfg for check_nrpe

```bash
define command{
	command_name 	check_nrpe
	command_line 	$USER1$/check_nrpe -H $HOSTADDRESS$ -c $ARG1$
}
```

5 - Edit the configuration file of the machines to monitor and edit service with check_local command to check_nrpe. For example:

```bash
define service {

    use                     local-service           ; Name of service template to use
    host_name               maquina2
    check_interval          1
    service_description     Current Load
    check_command           check_nrpe!check_load -a 5.0,4.0,3.0 10.0,6.0,4.0
}
```

6 - Restart Nagios
```sudo systemctl restart nagios```

## Automatic Recovery

* Configure Tomcat (Monitored Machine):

1 - Install tomcat 7, including tomcat7-docs tomcat7-adminm tomcat7-examples 

```sudo apt-get -y install tomcat7```

```sudo apt-get -y install tomcat7-docs tomcat7-admin tomcat7-examples```

2 - Allow ssh

```sudo ufw allow ssh```

3 - Create roles manager-gui and admin-gui on the file tomcat-users.xml and create a user with those roles

```sudo sed -i "s#</tomcat-users>#  <role rolename=\\"manager-gui\\"/>\\n</tomcat-users>#" /etc/tomcat7/tomcat-users.xml```

```sudo sed -i "s#</tomcat-users>#  <role rolename=\\"admin-gui\\"/>\\n</tomcat-users>#" /etc/tomcat7/tomcat-users.xml```

```sudo sed -i "s#</tomcat-users>#  <user username=\\"admin\\" password=\\"secret\\" roles=\\"manager-gui,admin-gui\\"/>\\n</tomcat-users>#" /etc/tomcat7/tomcat-users.xml```

4 - Allow tomcat port on firewall

```sudo ufw allow 8080/tcp```

5 - Start Tomcat

```sudo systemctl start tomcat7```

6 - Enable tomcat on start of the machine (opcional)

```sudo systemctl enable tomcat7```

* Create contacts in nagios and notify services by e-mail (Monitoring Machine):

1 - Create a test gmail account (it is not recommend to use your own account) and in that account, enable the account access for less secure apps in security options of gmail, so that the email can be sent successfully.

2 - Install sendEmail
```sudo apt-get install -y sendemail```

3 - To use tls you will need to do 
```sudo apt-get install libio-socket-ssl-perl libnet-ssleay-perl perl```

4 - Add in /usr/local/nagios/etc/resource.cfg your email credentials (macro):
``` bash 
$USER5$=dummy_email@gmail.com #Changable
$USER7$=smtp.gmail.com:587
$USER9$=dummy_email@gmail.com #Changable
$USER10$=your_password #Changable
```

5 - In /usr/local/nagios/etc/objects/commands.cfg create (or edit) the commands "notify-host-by-email" and "notify-service-by-email" 
``` bash
define command{
	command_name	notify-host-by-email
	command_line	/usr/bin/printf "%b" "***** Nagios *****\n\nNotification Type: $NOTIFICATIONTYPE$\nHost: $HOSTNAME$\nState: $HOSTSTATE$\nAddress: $HOSTADDRESS$\nInfo: $HOSTOUTPUT$\n\nDate/Time: $LONGDATETIME$\n" | sendEmail -s $USER7$ -xu $USER9$ -xp $USER10$ -t $USER5$ -f $USER5$ -o tls=yes -u "** $NOTIFICATIONTYPE$ Host Alert: $HOSTNAME$ is $HOSTSTATE$ **" -m "***** Nagios *****\n\nNotification Type: $NOTIFICATIONTYPE$\nHost: $HOSTNAME$\nState: $HOSTSTATE$\nAddress: $HOSTADDRESS$\nInfo: $HOSTOUTPUT$\n\nDate/Time: $LONGDATETIME$\n"
}
 
define command{
	command_name	notify-service-by-email
	command_line	/usr/bin/printf "%b" "***** Nagios *****\n\nNotification Type: $NOTIFICATIONTYPE$\n\nService: $SERVICEDESC$\nHost: $HOSTALIAS$\nAddress: $HOSTADDRESS$\nState: $SERVICESTATE$\n\nDate/Time: $LONGDATETIME$\n\nAdditional Info:\n\n$SERVICEOUTPUT$" | sendEmail -s $USER7$ -xu $USER9$ -xp $USER10$ -t $CONTACTEMAIL$ -f $USER5$ -o tls=yes -u "** $NOTIFICATIONTYPE$ Service Alert: $HOSTALIAS$/$SERVICEDESC$ is $SERVICESTATE$ **" -m "***** Nagios *****\n\nNotification Type: $NOTIFICATIONTYPE$\n\nService: $SERVICEDESC$\nHost: $HOSTALIAS$\nAddress: $HOSTADDRESS$\nState: $SERVICESTATE$\n\nDate/Time: $LONGDATETIME$\n\nAdditional Info:\n\n$SERVICEOUTPUT$"
}
```

6 - Create a contact in /usr/local/nagios/etc/objects/contact.cfg
``` bash
define contact {
        contact_name                            nelson
        alias                                   nelson
        email                                   your_email@gmail.com
        service_notification_period             24x7
        service_notification_options            w,u,c,r,f,s
        service_notification_commands           notify-service-by-email
        host_notification_period                24x7
        host_notification_options               d,u,r,f,s
        host_notification_commands              notify-host-by-email
}
```

7 - In the file /usr/local/nagios/etc/objects/maquina2.cfg add the information relative to the service tomcat to get notified:
``` bash
define service {
        use                                     local-service
        host_name                               maquina2
        service_description                     HTTP-Tomcat-8080
        check_command                           check_http_port
        check_interval                          1
        _port_number                            8080
        contacts                                nelson         
}
```

8 - Restart Nagios
```sudo systemctl restart nagios```

### Configure Nagios for the automatic recovery of Tomcat

1 - Create a script to restart Tomcat in the monitored machine

/usr/local/bin/restart_tomcat
```sudo systemctl restart tomcat7```

2 - Edit nrpe.cfg in the monitored machine and append the code:
```command[restart_tomcat]=/usr/local/bin/restart_tomcat```

3 - In the monitoring machine set enable_event_handlers=1 in the /usr/local/nagios/etc/nagios.cfg and add the event_handler:
```bash
event_handler_enabled           1
event_handler                   restart-tomcat
```

4 - Add a command to commands.cfg with the event handler
```bash
define command{
    command_name restart-tomcat
    command_line    /usr/local/nagios/libexec/restart-tomcat  $SERVICESTATE$ $SERVICESTATETYPE$ $SERVICEATTEMPT$ $HOSTADDRESS$
}
```

5 - Create a script to restart-tomcat in /usr/local/nagios/libexec/restart-tomcat in the monitoring machine:

```bash
#!/bin/sh
#
# Event handler script for restarting the web server on the local machine
#
# Note: This script will only restart the web server if the service is
#       retried 3 times (in a "soft" state) or if the web service somehow
#       manages to fall into a "hard" error state.
#

case "$1" in
OK)
	# The service just came back up, so don't do anything...
	;;
WARNING)
	# We don't really care about warning states, since the service is probably still running...
	;;
UNKNOWN)
	# We don't know what might be causing an unknown error, so don't do anything...
	;;
CRITICAL)
	# Aha!  The HTTP service appears to have a problem - perhaps we should restart the server...
	# Is this a "soft" or a "hard" state?
	case "$2" in

	# We're in a "soft" state, meaning that Nagios is in the middle of retrying the
	# check before it turns into a "hard" state and contacts get notified...
	SOFT)

		# What check attempt are we on?  We don't want to restart the web server on the first
		# check, because it may just be a fluke!
		case "$3" in

		# Wait until the check has been tried 3 times before restarting the web server.
		# If the check fails on the 4th time (after we restart the web server), the state
		# type will turn to "hard" and contacts will be notified of the problem.
		# Hopefully this will restart the web server successfully, so the 4th check will
		# result in a "soft" recovery.  If that happens no one gets notified because we
		# fixed the problem!
		2)
			echo -n "Restarting Tomcat service (3rd soft critical state)..."
			# Call the init script to restart the HTTPD server
			/usr/local/nagios/libexec/check_nrpe -H $4 -c restart_tomcat
			;;
			esac
		;;

	# The HTTP service somehow managed to turn into a hard error without getting fixed.
	# It should have been restarted by the code above, but for some reason it didn't.
	# Let's give it one last try, shall we?  
	# Note: Contacts have already been notified of a problem with the service at this
	# point (unless you disabled notifications for this service)
	HARD)
		echo -n "Restarting Tomcat service..."
		# Call the init script to restart the HTTPD server
		/usr/local/nagios/libexec/check_nrpe -H $4 -c restart_tomcat
		;;
	esac
	;;
esac
exit 0
```

6 - Give the restart-tomcat read and execution permissions:
```sudo chmod 755 /usr/local/nagios/libexec/restart-tomcat```

7 - Restart nagios
````sudo systemctl restart nagios```

# Zabbix

## Step 1: Install Apache2 HTTP Server on Ubuntu

```sudo apt update```

```sudo apt install apache2```

(Optional)

```sudo systemctl stop apache2.service```

```sudo systemctl start apache2.service```

```sudo systemctl enable apache2.service```

## Step 2: Install MariaDB Database Server

```sudo apt-get install mariadb-server mariadb-client```

(Optional)

```sudo systemctl stop mysql.service```

```sudo systemctl start mysql.service```

```sudo systemctl enable mysql.service```

After that, run the commands below to secure MariaDB server by creating a root password and disallowing remote root access.

```sudo mysql_secure_installation```

When prompted, answer the questions below by following the guide.

* Enter current password for root (enter for none): Just press the Enter
* Set root password? [Y/n]: Y
* New password: Enter password
* Re-enter new password: Repeat password
* Remove anonymous users? [Y/n]: Y
* Disallow root login remotely? [Y/n]: Y
* Remove test database and access to it? [Y/n]:  Y
* Reload privilege tables now? [Y/n]:  Y

Open MariaDB config file:

```sudo nano /etc/mysql/mariadb.conf.d/50-server.cnf```

Add the following lines:

```innodb_file_format = Barracuda```

```innodb_large_prefix = 1```

```innodb_default_row_format = dynamic```

Then restart MariaDB service.

## Step 3: Install PHP 7.2 and Related Modules

```sudo apt-get install software-properties-common```

```sudo add-apt-repository ppa:ondrej/php```

```sudo apt update```

```sudo apt install php7.2 libapache2-mod-php7.2 php7.2-common php7.2-mysql php7.2-gmp php7.2-curl php7.2-intl php7.2-mbstring php7.2-xmlrpc php7.2-mysql php7.2-gd php7.2-xml php7.2-cli php7.2-zip```

After that open PHP config file

```sudo nano /etc/php/7.2/apache2/php.ini```

Add the following lines:

```file_uploads = On```

```allow_url_fopen = On```

```short_open_tag = On```

```memory_limit = 256M```

```upload_max_filesize = 100M```

```max_execution_time = 360```

```date.timezone = Europe/Lisbon```

After that restart apache:

```sudo systemctl restart apache2.service```

## Step 4: Create Zabbix Database

Login in MariaDB database server:

```sudo mysql -u root -p```

After that creat a database called zabbix, a user called zabbixuser with a new password. Grant the user full access to the database and, finally, save and exit:

```CREATE DATABASE zabbix character set utf8 collate utf8_bin;```

```CREATE USER 'zabbixuser'@'localhost' IDENTIFIED BY 'new_password_here';```

```GRANT ALL ON zabbix.* TO 'zabbixuser'@'localhost' IDENTIFIED BY 'user_password_here' WITH GRANT OPTION;```

```FLUSH PRIVILEGES;```

```EXIT;```

## Step 5: Install Zabbix

Install the necessarie packages

```cd /tmp```

```wget https://repo.zabbix.com/zabbix/4.0/ubuntu/pool/main/z/zabbix-release/zabbix-release_4.0-2+bionic_all.deb```

```wget https://repo.zabbix.com/zabbix/4.0/ubuntu/pool/main/z/zabbix-release/zabbix-release_4.0-2+xenial_all.deb```

```sudo dpkg -i zabbix-release_4.0-2+bionic*.deb```

```sudo dpkg -i zabbix-release_4.0-2+xenial*.deb```

```sudo apt update```

```sudo apt install zabbix-server-mysql zabbix-agent zabbix-frontend-php php7.2-bcmath```

## Step 6: Configure Zabbix

Go to the zabbix server conf file and replace the DBName, DBUser and DBPassword for your configurations made in the previous step:

```sudo nano /etc/zabbix/zabbix_server.conf```

In the zabbix agent config file add a hostname for your server (add line "Hostname=zabbix.example.com"):

````sudo nano /etc/zabbix/zabbix_agentd.conf```

Import initial schema and data for the server with MySQL and then restart zabbix server and apache2:

```zcat /usr/share/doc/zabbix-server-mysql/create.sql.gz | mysql -u zabbixuser -p zabbix```

```sudo systemctl restart zabbix-server```

```sudo systemctl enable zabbix-server```

```sudo systemctl reload apache2.service```

Open Zabbix setup page (http://zabbix.example.com/zabbix), verify if all requirements are met and configure the server details with your configurations previously created.

If you want to login you can use Username:admin and Password:zabbix